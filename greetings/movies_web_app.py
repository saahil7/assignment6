import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies (ID INT UNSIGNED NOT NULL AUTO_INCREMENT, YEAR INT, TITLE TEXT , DIRECTOR TEXT, ACTOR TEXT, RELEASE_DATE TEXT, RATING DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

#make case insensitive
@app.route('/insert', methods=['POST'])
def insert():

    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release = request.form['release']
    rating = request.form['rating']
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("INSERT INTO movies (YEAR, TITLE, DIRECTOR, ACTOR, RELEASE_DATE, RATING) values ('" + year + "', LOWER('" + title + "'), LOWER('" + director + "'), LOWER('" + actor + "'), LOWER('" + release + "'), '" + rating + "')")
        cnx.commit()
        messages = "%s successfully inserted!" % title
    except Exception as exp:
        print(exp)
        messages = "Movie %s could not be inserted " % (title, str(exp))

    return render_template('index.html', messages=messages)

@app.route('/update', methods=['POST'])
def update():
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release = request.form['release']
    rating = request.form['rating']
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("UPDATE movies SET YEAR = '" + year + "', TITLE = LOWER('" + title + "'), DIRECTOR = LOWER('" + director + "'), ACTOR = LOWER('" + actor + "'), RELEASE_DATE = LOWER('" + release + "'), RATING = '" + rating + "' WHERE TITLE = LOWER('" + title + "')")
        cnx.commit()
        messages = "Movie %s successfully updated!" % title
    except Exception as exp:
        print(exp)
        messages = "Error updating movie"
    return render_template('index.html', messages=messages)

@app.route('/')
def home():
    return render_template('index.html')

#make case insensitive
@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title = request.form['title']
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE title = LOWER('" + title + "')")
        entries = cur.fetchall()
        if (len(entries) == 0):
            messages = "movie %s does not exist" % title
            return render_template('index.html', messages = messages)
        cur.execute("DELETE FROM movies WHERE title = LOWER('" + title + "')")
        cnx.commit()
        messages = "movie %s deleted" % title
    except Exception as exp:
        messages = "Movie %s could not be deleted" % title

    return render_template('index.html', messages = messages)


@app.route('/search_movies', methods=['POST'])
def search_movies():
    print("Received request.")
    actor = request.form['actor']
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT TITLE, YEAR, ACTOR FROM movies WHERE ACTOR = LOWER('" + actor + "')")
        entries = list(cur.fetchall())
        if len(entries) == 0:
            messages = "no movies found for %s" % actor
            return render_template('index.html', messages = messages)
    except Exception as exp:
        print(exp)
        messages = "no movies found for %s" % actor

    return render_template('index.html', entries = entries)


@app.route('/print_stats_max', methods=['GET'])
def print_stats_max():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT YEAR, TITLE, ACTOR, DIRECTOR, RATING FROM movies WHERE RATING=(SELECT MAX(RATING) from movies)")
        entries2 = list(cur.fetchall())
    except Exception as exp:
        print(exp)
        messages2 = "Error getting movie"
        return render_template('index.html', messages=messages)

    return render_template('index.html', entries2 = entries2)

@app.route('/print_stats_min', methods=['GET'])
def print_stats_min():
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
        cur = cnx.cursor()
        cur.execute("SELECT YEAR, TITLE, ACTOR, DIRECTOR, RATING FROM movies WHERE RATING=(SELECT MIN(RATING) from movies)")
        entries2 = list(cur.fetchall())
    except Exception as exp:
        print(exp)
        messages2 = "Error getting movie"
        return render_template('index.html', messages=messages2)

    return render_template('index.html', entries2 = entries2)

if __name__ == "__main__":
    app.debug = True
    create_table()
    app.run(host='0.0.0.0')
